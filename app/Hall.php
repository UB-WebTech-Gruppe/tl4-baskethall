<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hall extends Model
{
    protected $fillable = [
        'name',
        'address',
        'seat_count',
        'entrance_exit_count'
    ];

    /**
     * Specifies the 1:n relationship to matches
     */
    public function matches() {
        return $this->hasMany("App\Match");
    }
}
