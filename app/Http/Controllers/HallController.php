<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Hall;

class HallController extends Controller {
    // get all halls
    public function index() {
        return view("halls")->with("halls", Hall::all("id", "name", "address", "seat_count", "entrance_exit_count"));
    }


    public function addHall() {
        $newHall = new Hall();
        return view("hallForm")->with("hall", $newHall);
    }

    public function editHall($hall_id) {
        $editHall = Hall::find($hall_id);
        return view("hallForm")->with("hall", $editHall);
    }

    public function saveHall(Request $req) {
        $req->validate([
            "name" => "required|unique:halls,name, " . $req->hall_id . ".|min:5",
            "address" => "required|min:10",
            "seat_count" => "required|numeric",
            "entrance_exit_count" => "required|numeric"
        ]);

        $hall_id = $req->hall_id;

        $name = $req->name;
        $address = $req->address;
        $seat_count = $req->seat_count;
        $entrance_exit_count = $req->entrance_exit_count;

        if ($hall_id) {
            Hall::find($hall_id)->update([
                "name" => $name,
                "address" => $address,
                "seat_count" => $seat_count,
                "entrance_exit_count" => $entrance_exit_count
            ]);
        } else {
            Hall::create([
                "name" => $name,
                "address" => $address,
                "seat_count" => $seat_count,
                "entrance_exit_count" => $entrance_exit_count
            ]);
        }

        return redirect()->route("home");
    }

    // delete hall with given id
    public function deleteHall($hall_id) {
        Hall::find($hall_id)->delete();

        return redirect()->route("home");
    }
}
