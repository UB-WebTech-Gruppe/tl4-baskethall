<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Match;
use App\Hall;

class MatchController extends Controller {
    
    // get all matches
    public function index() {        
        $matches = Match::all("name_home_team", "name_guest_team", "datetime", "hall_id");
        $matchesByDate = $matches->sortBy("datetime")->groupBy(function ($date) {
            return Carbon::parse($date->datetime)->format("d.m.Y"); // group by days
        });

        return view("dateView")->with("matches", $matchesByDate);
    }

    public function addMatch() {
        $newMatch = new Match();
        return view("addMatchForm")->with("match", $newMatch);
    }

    public function saveMatch(Request $req) {
        $hall = Hall::find($req->hall_id);
        $req->validate([
            "name_home_team" => "required|min:7",
            "name_guest_team" => "required|min:7",
            "hall_id" => "required|numeric",
            "datetime" => [
                "required",
                "date:'d.m.Y H:i'",
                "after_or_equal:now",
                function($attribute, $value, $fail)  use ($hall){
                    $dt = Carbon::parse($value);

                    if($dt->hour < 10 or ($dt->hour >= 22 and $dt->minute > 0)) {
                        $fail("The input :attribute hour must not be before 10am or after 10pm!");
                    }
                    
                    $datetimes = DB::table("matches")->where("hall_id" , "=", $hall->id)
                        ->whereRaw("DATE(datetime) = ?", [$dt->toDateString()])->pluck("datetime");

                    if($datetimes->isEmpty()) {
                        return;
                    }

                    foreach($datetimes as $datetime) {
                        $datetime = Carbon::parse($datetime);

                        // get the hour difference as an absolute value
                        $hourDiff = $dt->diffInHours($datetime, true);

                        if($hourDiff < 4) {
                            $fail("The input :attribute is not 4 hours before or after the " . $datetime->toTimeString() . " appointment in the same hall!");
                        }
                    }
                }
            ]
        ]);

        $hall = Hall::find($req->hall_id);

        $name_home_team = $req->name_home_team;
        $name_guest_team = $req->name_guest_team;
        $datetime = Carbon::parse($req->datetime)->format("Y-m-d H:i:s");

        Match::create([
            "hall_id" => $hall->id,
            "name_home_team" => $name_home_team,
            "name_guest_team" => $name_guest_team,
            "datetime" => $datetime
        ]);

        return redirect()->route("home");
    }

    /**
     * delete `Match` with given id
     */
    public function deleteMatch($match_id) {
        Match::find($match_id)->delete();

        return redirect()->route("home");
    }
}
