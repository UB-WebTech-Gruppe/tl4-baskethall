@extends("main")

@section("content")

<div class="container p-3 ml-0">
    <h2>Alle Termine im Überblick</h2>

    @foreach($matches as $date => $matches)
        <h3>{{ $date }} <span class="badge badge-secondary">{{ count($matches) }}</span></h3>

        <table class="table">
            <thead class="{{ $loop->index % 2 == 0 ? 'thead-dark' : 'thead-light' }}">
                    <tr>
                        <th scope="col">Heimmannschaft</th>
                        <th scope="col">Gastmannschaft</th>
                        <th scope="col">Uhrzeit</th>
                        <th scope="col">Austragungsort</th>                                                    
                    </tr>
            </thead>

            <tbody>
                @foreach($matches as $match)        
                    <tr>
                        <td>{{ $match->name_home_team }}</td>
                        <td>{{ $match->name_guest_team }}</td>
                        <td>{{ \Carbon\Carbon::parse($match->datetime)->format("H:i") }}</td>
                        <td>{{ $match->hall->name }}</td>                        
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endforeach
</div>

@endsection
