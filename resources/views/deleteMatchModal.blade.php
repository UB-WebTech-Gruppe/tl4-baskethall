<div class="modal fade" id="deleteMatchModal{{ $loop->parent->iteration }}{{ $loop->iteration }}" tabindex="-1" role="dialog" aria-labelledby="deleteMatchModalLabel{{ $loop->parent->iteration }}{{ $loop->iteration }}" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="deleteMatchModalLabel{{ $loop->parent->iteration }}{{ $loop->iteration }}">Löschen bestätigen</h5>

              <button type="button" class="close" data-dismiss="modal" aria-label="Abbrechen">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <div class="modal-body">
              <p>Möchtest du die Partie wirklich löschen?<br/>
                <strong>Das kann nicht rückgängig gemacht werden!</strong>
              </p>
            </div>

            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Abbrechen</button>

              <form method="POST" action="{!! route("deleteMatch", ["id" => $match->id]) !!}">
                {{ method_field("DELETE") }}
                {{ csrf_field() }}
                <button type="submit" class="btn btn-danger">Löschen</button>
              </form>
            </div>
        </div>
    </div>
</div>
