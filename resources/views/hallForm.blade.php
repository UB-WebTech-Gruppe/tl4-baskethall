@extends("main")

@section("content")

<div class="container p-3 ml-0">
    <h2>Halle {{ $hall->id ? $hall->name . ' editieren' : ' neu anlegen' }}</h2>

    @if ($errors->any())
    <div class="errors col-12">
        @foreach ($errors->all() as $error)
        <div class="alert alert-danger" role="alert">{{ $error }}</div>
        @endforeach
    </div>
    @endif

    <form method="POST" action="{!! route("saveHall") !!}">
        <input type="hidden" name="hall_id" value="{{ $hall->id }}" />
        <div class="form-row mb-3">
            {{ csrf_field() }}
            <div class="col-md-6">
                <input class="form-control" type="text" name="name" placeholder="Hallenname" value="{{ old('name') ? old('name') : $hall->name }}" required />
            </div>
            <div class="col-md-6">
                <input class="form-control" type="number" name="entrance_exit_count" placeholder="Anzahl Ein-/Ausgänge" value="{{ old('entrance_exit_count') ? old('entrance_exit_count') : $hall->entrance_exit_count }}" required />
            </div>
        </div>

        <div class="form-row mb-3">
            <div class="col-md-8">
                <input class="form-control" type="text" name="address" placeholder="Adresse" value="{{ old('address') ? old('address') : $hall->address }}" required />
            </div>

            <div class="col-md-4">
                <input class="form-control" type="number" name="seat_count" placeholder="Sitzplätze" value="{{ old('seat_count') ? old('seat_count') : $hall->seat_count }}" required />
            </div>
        </div>

        <button type="submit" class="btn btn-primary">Hinzufügen</button> oder <a class="btn btn-secondary" href="{!! route("home") !!}" role="button">Abbrechen</a>
    </form>
</div>

@endsection
