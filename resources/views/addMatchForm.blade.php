@extends("main")

@section("content")

<div class="container p-3 ml-0">
  <h2>Neue Partie anlegen</h2>

    @if ($errors->any())
      <div class="errors col-12">
        @foreach ($errors->all() as $error)
          <div class="alert alert-danger" role="alert">{{ $error }}</div>
        @endforeach
      </div>
    @endif

    <form method="POST" action="{!! route("saveMatch") !!}">
      <div class="form-row mb-3">
        {{ csrf_field() }}
        <div class="col-md-6">
            <input class="form-control" type="text" name="name_home_team" placeholder="Heimmannschaft" value="{{ old('name_home_team') }}" required />
        </div>
        <div class="col-md-6">
            <input class="form-control" type="text" name="name_guest_team" placeholder="Gastmannschaft" value="{{ old('name_guest_team') }}" required />
        </div>
      </div>

      <div class="form-row mb-3">
          <div class="col-md-4">
              <select class="form-control" name="hall_id">
                @foreach(\App\Hall::all("id", "name") as $hall)
                  <option value="{{ $hall->id }}">{{ $hall->name }}</option>
                @endforeach
              </select>
          </div>
          <div class="col-md-8">
              <input class="form-control" type="text" name="datetime" placeholder="DD.MM.YYYY HH:MM" value="{{ old('datetime') }}" required />
          </div>
      </div>

      <button type="submit" class="btn btn-primary">Hinzufügen</button> oder <a class="btn btn-secondary" href="{!! route("home") !!}" role="button">Abbrechen</a>
    </form>
</div>

@endsection
