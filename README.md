# TL4-baskethall

A `CRUD` web application based on `PHP`, `SQL`, `Bootstrap` and the `Laravel` framework that can be used to manage basketball halls and matches.

## Running

Clone the repository and run ```composer install``` in the directory to re-create the `vendor` directory.  
Then copy the `.env.example` file into the same directory with the name `.env` and configure your database system credentials.  
Run `php artisan key:generate` and `php artisan migrate:fresh --seed` to create the tables and seed them with sample data, after that you finally can start the embedded web server with `php artisan serve` and browse the page at `127.0.0.1:8000`.

## License

Licensed under the [Apache License 2.0][license].

[license]: LICENSE
