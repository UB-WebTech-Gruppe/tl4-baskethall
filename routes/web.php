<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get("/", "HallController@index")->name("home");
Route::get("/addHall", "HallController@addHall")->name("addHall");
Route::get("/editHall/{hall_id}", "HallController@editHall")->name("editHall");
Route::delete("/deleteHall/{hall_id}", "HallController@deleteHall")->name("deleteHall");
Route::post("/saveHall", "HallController@saveHall")->name("saveHall");

Route::get("/addMatch", "MatchController@addMatch")->name("addMatch");
Route::post("/saveMatch", "MatchController@saveMatch")->name("saveMatch");
Route::delete("/deleteMatch/{match_id}", "MatchController@deleteMatch")->name("deleteMatch");

Route::get("/dateview", "MatchController@index")->name("dateview");
