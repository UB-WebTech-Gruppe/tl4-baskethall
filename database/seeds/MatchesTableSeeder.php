<?php

use Illuminate\Database\Seeder;
use App\Match;

class MatchesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        Match::create([
            "hall_id" => 1,
            "name_home_team" => "Brose Baskets",
            "name_guest_team" => "Ratiopharm Ulm",
            "datetime" => "2018-07-10 10:00"
        ]);
        Match::create([
            "hall_id" => 1,
            "name_home_team" => "Brose Baskets",
            "name_guest_team" => "Bayer Giants Leverkusen",
            "datetime" => "2018-07-10 20:00"
        ]);
        Match::create([
            "hall_id" => 1,
            "name_home_team" => "Brose Baskets",
            "name_guest_team" => "Alba Berlin",
            "datetime" => "2018-07-15 20:00"
        ]);
        Match::create([
            "hall_id" => 2,
            "name_home_team" => "FC Bayern Basketball",
            "name_guest_team" => "Alba Berlin",
            "datetime" => "2018-07-11 14:00"
        ]);
        Match::create([
            "hall_id" => 2,
            "name_home_team" => "FC Bayern Basketball",
            "name_guest_team" => "Gießen 46ers",
            "datetime" => "2018-07-11 18:00"
        ]);
    }
}
